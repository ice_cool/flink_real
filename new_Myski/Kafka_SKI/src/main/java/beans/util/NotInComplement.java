package beans.util;

import org.apache.kafka.common.protocol.types.Field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Package: beans.util
 * @ClassName: NotInComplement
 * @Author: ski_starLight
 * @CreateTime: 2021/4/8 17:06
 * @Description: 判断一个StringBuilder 中的list中的值
 */
public class NotInComplement {

    public static void main(String[] args) {
        HashMap<String, Object> map1 = new HashMap<>();
        HashMap<String, Object> map2 = new HashMap<>();
        HashMap<String, Object> map3 = new HashMap<>();
        map1.put("ui_pol_apply_date", "2021-03-09");
        map1.put("channel_rename", "全部");
        map1.put("insured_person_amt", 2);

        map2.put("ui_pol_apply_date", "2021-03-09");
        map2.put("channel_rename", "圆心");
        map2.put("insured_person_amt", 1);

        map3.put("ui_pol_apply_date", "2021-03-09");
        map3.put("channel_rename", "人保财");
        map3.put("insured_person_amt", 1);

        ArrayList<Map<String, Object>> list = new ArrayList<>();

        list.add(map1);
        list.add(map2);
        list.add(map3);

        StringBuilder sb = new StringBuilder();
        // TODO 将渠道名称都拿出来放到sb中
        for (Map<String, Object> stringObjectMap : list) {
            sb.append(stringObjectMap.get("channel_rename"));
        }
        // TODO 将所有渠道集合一起
        String all_channel_rename = "圆心,平安养老,人保财,太保财,国寿康,全部";
        String[] split_channel_rename = all_channel_rename.split(",");
        // TODO 创建结果集合
        List<Map<String, Object>> resultData = new ArrayList<>(6);
        // TODO 循环all_channel_rename 每个渠道,是否在sb中,不在就添加0
        for (String s : split_channel_rename) {
            if (sb.toString().contains(s) == false) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("channel_rename", s);
                map.put("insured_person_amt", 0);
                resultData.add(map);
            }
        }
        for (Map<String, Object> stringObjectMap : list) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("channel_rename", stringObjectMap.get("channel_rename"));
            map.put("insured_person_amt", stringObjectMap.get("insured_person_amt"));
            resultData.add(map);
        }

        System.out.println(resultData);



    }


}
