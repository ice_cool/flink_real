import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Properties;

/**
 * @Package: PACKAGE_NAME
 * @ClassName: KakfaBridge
 * @Author: ski_starLight
 * @CreateTime: 2021/3/31 13:55
 * @Description:
 */
public class KakfaBridge {
    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.3.102:9092");
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "test");
        DataStreamSource<String> firstKafkaSource = env.addSource(new FlinkKafkaConsumer<String>("first", new SimpleStringSchema(), properties));
        firstKafkaSource.print();
        env.execute();


    }
}
