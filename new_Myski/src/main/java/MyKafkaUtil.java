import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Properties;

/**
 * @Author Ski_StarLight
 * @Date 2021-03-31 14:19
 * @Description:
 */
public class MyKafkaUtil {

    private static String kafkaServer = "192.168.3.102:9092";
    public static FlinkKafkaConsumer<String> getKafkaConsumer(String topic,String groupId){

        Properties prop = new Properties();
        prop.setProperty(ConsumerConfig.GROUP_ID_CONFIG,groupId);
        prop.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,kafkaServer);
        return new FlinkKafkaConsumer<String>(topic,new SimpleStringSchema(),prop);
    }
}
