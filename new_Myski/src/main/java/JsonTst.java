import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @Package: PACKAGE_NAME
 * @ClassName: JsonTst
 * @Author: ski_starLight
 * @CreateTime: 2021/3/31 14:45
 * @Description:
 */
public class JsonTst {
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("\"yyyy-MM-dd HH:mm:00\"");
        String josn = "{\"data\":[{\"id\":\"2\",\"last_modify_time\":\"2021-03-31 10:45:21\"}],\"database\":\"Oms\",\"es\":1617158721111,\"id\":342,\"isDdl\":false,\"mysqlType\":{\"id\":\"bigint(20) unsigned\",\"last_modify_time\":\"timestamp\"},\"old\":[{\"last_modify_time\":\"2021-03-30 19:15:49\"}],\"pkNames\":[\"id\"],\"sql\":\"\",\"sqlType\":{\"id\":-5,\"last_modify_time\":93},\"table\":\"mall_order_info\",\"ts\":1617158721213,\"type\":\"UPDATE\"}";

        JSONObject jsonObject = JSONObject.parseObject(josn);

        Long es = jsonObject.getLong("es");
        JSONArray data = jsonObject.getJSONArray("data");
        String id1 = jsonObject.getString("id");

        String format = simpleDateFormat.format(es);
        Long parse = simpleDateFormat.parse(format).getTime();
        System.out.println(format);
        System.out.println(parse);

    }
//    1617158721111
//    1617158700000
}
