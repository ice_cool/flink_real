import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import scala.Int;
import sun.plugin.javascript.navig.Array;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * @Package: PACKAGE_NAME
 * @ClassName: JsonTst
 * @Author: ski_starLight
 * @CreateTime: 2021/3/31 14:45
 * @Description:
 */
public class JsonTest2 {
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");

    public static void main(String[] args) throws ParseException {
        String josn = "{\n" +
                "    \"success\": true,\n" +
                "    \"code\": 10000,\n" +
                "    \"msg\": \"成功\",\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"age_band\": \"0-20\",\n" +
                "            \"insure_uqe\": 26738\n" +
                "        },\n" +
                "        {\n" +
                "            \"age_band\": \"20-40\",\n" +
                "            \"insure_uqe\": 73448\n" +
                "        },\n" +
                "        {\n" +
                "            \"age_band\": \"40-60\",\n" +
                "            \"insure_uqe\": 109267\n" +
                "        },\n" +
                "        {\n" +
                "            \"age_band\": \"60-80\",\n" +
                "            \"insure_uqe\": 66758\n" +
                "        },\n" +
                "        {\n" +
                "            \"age_band\": \"80-100\",\n" +
                "            \"insure_uqe\": 6562\n" +
                "        }\n" +
                "    ],\n" +
                "    \"exceptionMessage\": null,\n" +
                "    \"exceptionStackTrace\": null\n" +
                "}";
        JSONObject jsonObject = JSONObject.parseObject(josn);
        JSONArray data = jsonObject.getJSONArray("data");
        Integer sum = 0;
        int[] a = new int[data.size()];
        for (int i = 0; i < data.size(); i++) {
            a[i] = JSONObject.parseObject(data.getString(i)).getInteger("insure_uqe");
        }
        sum = Arrays.stream(a).sum();
        System.out.println(sum);
        for (int i = 0; i < a.length; i++) {
            Double i1 = a[i]*1.00 / sum;


            System.out.println(i1);
            System.out.println(a[i]);

        }
        System.out.println(jsonObject.toString());


//        String string = data.getString(0);
//        JSONObject jsonObject1 = JSONObject.parseObject(data.getString(0));
//        String age_band = jsonObject1.getString("age_band");
//        System.out.println(string);
//        System.out.println(data);
//        System.out.println(jsonObject1);
//        System.out.println(age_band);


        // 循环数组求和

    }

}
