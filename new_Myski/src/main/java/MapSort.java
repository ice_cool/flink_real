import java.util.*;

/**
 * @Package: PACKAGE_NAME
 * @ClassName: MapSort
 * @Author: ski_starLight
 * @CreateTime: 2021/4/6 13:57
 * @Description:
 */
public class MapSort {

    public static void main(String[] args) {

        HashMap<String, Object> resultHashMap1 = new HashMap<>();
        HashMap<String, Object> resultHashMap2 = new HashMap<>();
        HashMap<String, Object> resultHashMap3 = new HashMap<>();
        HashMap<String, Object> resultHashMap4 = new HashMap<>();
        HashMap<String, Object> resultHashMap5 = new HashMap<>();
        List<Map<String, Object>> allResult = new ArrayList<>();

        resultHashMap1.put("region", "河北");
        resultHashMap1.put("insure_amt", "51185202");
        resultHashMap1.put("insure_cnt", "461598");

        resultHashMap2.put("region", "天津");
        resultHashMap2.put("insure_amt", "8278660");
        resultHashMap2.put("insure_cnt", "78875");

        resultHashMap3.put("region", "兰州");
        resultHashMap3.put("insure_amt", "21023472");
        resultHashMap3.put("insure_cnt", "252918");

        resultHashMap4.put("region", "海南");
        resultHashMap4.put("insure_amt", "12057562");
        resultHashMap4.put("insure_cnt", "263599");

        resultHashMap5.put("region", "蚌埠");
        resultHashMap5.put("insure_amt", "311481");
        resultHashMap5.put("insure_cnt", "2121");

        allResult.add(resultHashMap1);
        allResult.add(resultHashMap2);
        allResult.add(resultHashMap3);
        allResult.add(resultHashMap5);
        allResult.add(resultHashMap4);
//        for (Map<String, Object> map1 : allResult) {
//            System.out.println(map1.get("insure_cnt"));
//        }
        System.out.println(allResult);
            Collections.sort(allResult, new Comparator<Map<String, Object>>() {
                @Override
                public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                    Integer o1Value = Integer.valueOf(o1.get("insure_amt").toString());
                    Integer o2Value = Integer.valueOf(o2.get("insure_amt").toString());
                    return o1Value<o2Value ? 1:-1;
                }
            });

        System.out.println(allResult);

       for (Map<String, Object> map1 : allResult) {
            System.out.println(map1.get("insure_amt"));
        }
    }
}
