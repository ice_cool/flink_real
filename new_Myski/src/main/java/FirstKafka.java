import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.util.Collector;

import java.text.SimpleDateFormat;


/**
 * @Author Ski_StarLight
 * @Date 2021-03-31 14:16
 * @Description:
 */
public class FirstKafka {
    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        FlinkKafkaConsumer<String> kafkaConsumer = MyKafkaUtil.getKafkaConsumer("first", "test");

        DataStreamSource<String> kafkaDS = env.addSource(kafkaConsumer);

        kafkaDS.print();

        SingleOutputStreamOperator<String> fetchCanalMessageCnt = kafkaDS
                .map(new MapFunction<String, Tuple2<Long, JSONObject>>() {
                    @Override
                    public Tuple2<Long, JSONObject> map(String s) throws Exception {
                        JSONObject jsonObject = JSONObject.parseObject(s);

                        String es = simpleDateFormat.format((jsonObject.getLong("es")));
                        long time = simpleDateFormat.parse(es).getTime();


                        return new Tuple2(time, jsonObject);

                    }
                })
                .assignTimestampsAndWatermarks(WatermarkStrategy.<Tuple2<Long, JSONObject>>forMonotonousTimestamps()
                        .withTimestampAssigner(new SerializableTimestampAssigner<Tuple2<Long, JSONObject>>() {
                            @Override
                            public long extractTimestamp(Tuple2<Long, JSONObject> stringJSONObjectMap, long l) {

                                return stringJSONObjectMap.f1.getLong("es");
                            }
                        }))
                .keyBy(new KeySelector<Tuple2<Long, JSONObject>, Long>() {

                    @Override
                    public Long getKey(Tuple2<Long, JSONObject> longJSONObjectTuple2) throws Exception {
                        return longJSONObjectTuple2.f0;
                    }
                })
                .window(TumblingEventTimeWindows.of(Time.minutes(1)))
                .aggregate(new MyAggregateFunction(), new MyWindowFunction());
        fetchCanalMessageCnt.print();
        env.execute();

    }


    /**
     * @Author ski_wangxuteng
     * @Date 18:11 2021/3/30
     * @Param
     * @return
     * @Description 注册累加器, 来一条进行加一;
     **/
    private static class MyAggregateFunction implements AggregateFunction<Tuple2<Long, JSONObject>, Long, Long> {

        @Override
        public Long createAccumulator() {
            return 0L;
        }

        @Override
        public Long add(Tuple2<Long, JSONObject> longJSONObjectTuple2, Long aLong) {
            return aLong + 1;
        }

        @Override
        public Long getResult(Long aLong) {
            return aLong;
        }

        @Override
        public Long merge(Long aLong, Long acc1) {
            return aLong + acc1;
        }
    }

    private static class MyWindowFunction implements WindowFunction<Long, String, Long, TimeWindow> {

        @Override
        public void apply(Long aLong, TimeWindow timeWindow, Iterable<Long> iterable, Collector<String> collector) throws Exception {
            collector.collect(timeWindow.getStart() + "," + iterable.iterator().next() + "," + timeWindow.getEnd());
        }
    }
}