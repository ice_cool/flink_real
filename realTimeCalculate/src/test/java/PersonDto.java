import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: lxs
 * @create: 2020-10-21 15:42
 */
@Data
public class PersonDto implements Serializable {
    private static final long serialVersionUID = 9034047936980511637L;
    /**
     * 城市编码
     */
    private String cityCode;
    /**
     * 性别
     */
    private Integer userSex;
    /**
     * 被保人与投保人的关系
     */
    private Integer insuredRelation;
    /**
     * 年龄段
     */
    private Integer ageBand;
    /**
     * 最大最小年龄
     */
    private Integer minAndMaxAge;
    /**
     * 证件类型
     */
    private Integer userCardType;

    /**
     * 地区排名-参保人数
     */
    private Integer fromSourceCntLimit;


}
