import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @Package: PACKAGE_NAME
 * @ClassName: DateUtilsTest
 * @Author: ski_starLight
 * @CreateTime: 2021/3/22 17:37
 * @Description:
 */
public class DateUtilsTest {
    public static final String QUESTION_MARK = "\\?";

    public static void main(String[] args) {

        PersonDto personDto = new PersonDto();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE, -1);

        String format = simpleDateFormat.format(instance.getTime());

//        System.out.println(instance.getTime());
//        System.out.println(format);
//
        String sql =
                "SELECT CASE WHEN uip_user_card_type='passport' THEN '护照' " +
                        "WHEN uip_user_card_type='hongkongMacaoBackCard' THEN '港澳回乡证' " +
                        "WHEN uip_user_card_type='taiwanCompatriotsCard' THEN '台胞证' " +
                        "WHEN uip_user_card_type='identityCard' " +
                        "THEN '身份证' else '其他' " +
                        "end as user_card_type" +
                        ", count(1) AS insure_uqe " +
                        "FROM dwd.dwd_yxhb_cshmb_user_insure_person_df " +
                        "WHERE dt = '?' " +
                        "AND ui_pay_status IN ('payYet', 'partRefund') " +
                        "AND uip_insurance_status != 'cancelOrder' " +
                        "AND uip_personnel_type='insured' " +
                        "AND city_code='?' " +
                        "GROUP BY uip_user_card_type;";
        if (personDto != null) {
            String new_sql = sql
                    .replaceFirst(QUESTION_MARK, format)
                    .replaceFirst(QUESTION_MARK,"jicheng");
            System.out.println(new_sql);
        }else return;


    }


}
