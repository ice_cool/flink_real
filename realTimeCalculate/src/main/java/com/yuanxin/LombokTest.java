package com.yuanxin;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Package: com.yuanxin
 * @ClassName: LombokTest
 * @Author: ski_starLight
 * @CreateTime: 2021/3/12 17:01
 * @Description:
 */
@Data
@Getter
@Setter
public class LombokTest {

    private  String a ;
    private  String b ;
    private  String c ;
    private  String d ;


    public static void main(String[] args) {
        LombokTest lombokTest = new LombokTest();
        lombokTest.setA("dfd");

        System.out.println(lombokTest.getA());
    }
}

