package com.yuanxin.real_time;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * @Package: com.yuanxin.real_time
 * @ClassName: TestFile
 * @Author: ski_starLight
 * @CreateTime: 2021/3/12 11:19
 * @Description:
 */
public class BatchWordCount {


    public static void main(String[] args) throws Exception {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        String path = "D:\\circle_center\\工作\\ski\\realTimeCalculate\\src\\main\\resources\\words.txt";

        DataSource<String> inputDataSet = env.readTextFile(path);

        DataSet<Tuple2<String, Integer>> result = inputDataSet.flatMap(new MyFlatMap())
                .groupBy(0)
                .sum(1);

        result.print();
    }

    public static class MyFlatMap implements FlatMapFunction<String, Tuple2<String, Integer>> {


        public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception {
            String[] words = value.split(" ");

            for (String word : words) {

                out.collect(new Tuple2<String, Integer>(word, 1));
            }
        }
    }
}
