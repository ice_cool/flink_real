package com.yuanxin.real_time;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @Package: com.yuanxin.real_time
 * @ClassName: StreamWordCount
 * @Author: ski_starLight
 * @CreateTime: 2021/3/12 11:58
 * @Description:
 */
public class StreamWordCount {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        ParameterTool parameterTool = ParameterTool.fromArgs(args);
/*
        String host = parameterTool.get("host");
        int port  = parameterTool.getInt("port");

        DataStream<String> inputSocket = env.socketTextStream(host, port);
        */

        String path = "D:\\circle_center\\工作\\ski\\realTimeCalculate\\src\\main\\resources\\words.txt";

        DataStream<String> inputSocket = env.readTextFile(path);

//        DataStream<String> inputSocket = env.socketTextStream("localhost", 7777);

        DataStream<Tuple2<String, Integer>> sum = inputSocket.flatMap(new MyFlatMapFunction())
                .keyBy(0)
                .sum(1);


        sum.print();
        env.execute();

    }

    private static class MyFlatMapFunction implements FlatMapFunction<String, Tuple2<String, Integer>> {
        @Override
        public void flatMap(String s, Collector<Tuple2<String, Integer>> collector) throws Exception {
            String[] words = s.split(" ");

            for (String word : words) {
                collector.collect(new Tuple2<>(word, 1));

            }
        }
    }
}
