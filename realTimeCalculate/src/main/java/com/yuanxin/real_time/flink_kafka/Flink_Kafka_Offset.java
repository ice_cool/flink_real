package com.yuanxin.real_time.flink_kafka;

import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.util.Collector;

import java.util.Properties;

/**
 * @Package: com.yuanxin.real_time.flink_kafka
 * @ClassName: Flink_Kafka_Offset
 * @Author: ski_starLight
 * @CreateTime: 2021/3/30 11:45
 * @Description:
 */
public class Flink_Kafka_Offset {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "192.168.3.102:9092");
        properties.setProperty("group.id", "cxx_mysql_binlog");

        FlinkKafkaConsumer<String> consumer = new FlinkKafkaConsumer<>("first", new SimpleStringSchema(), properties);

        DataStreamSource<String> kafkaSource = env.addSource(consumer);
        kafkaSource.print();

        SingleOutputStreamOperator<Long> contMessage = kafkaSource
                .map(new MapFunction<String, JSONObject>() {
                    @Override
                    public JSONObject map(String value) throws Exception {
                        return JSONObject.parseObject(value);

                    }
                })
                // 处理乱序时间assignTimestampsAndWatermarks,1.12简化了
                .assignTimestampsAndWatermarks(WatermarkStrategy.<JSONObject>forMonotonousTimestamps().withTimestampAssigner(new SerializableTimestampAssigner<JSONObject>() {
                    @Override
                    public long extractTimestamp(JSONObject element, long recordTimestamp) {
                        return new Tuple2<JSONObject, Long>().f0.getLong("es");
                    }
                }))
                .timeWindowAll(Time.minutes(10))
                .aggregate(new MyAggregateFunction());
        contMessage.print();
        env.execute();


    }

    /**
     * @Author ski_wangxuteng
     * @Date 15:53 2021/3/30
     * @Description 使用processFunction 定义时间状态和一个int状态,每来一条数据就加一,并更新时间状态的值,定时器在第一个
     * 时间来的10min之后关闭,然后清空状态,重新注册定时器
     **/

    private static class TimerProcessFunction extends ProcessFunction<JSONObject, String> {

        private ValueState<Integer> messageCnt;
        private ValueState<Long> esTime;

        @Override
        public void open(Configuration parameters) throws Exception {
            messageCnt = getRuntimeContext().getState(new ValueStateDescriptor<Integer>("messageCnt", Integer.class));
            esTime = getRuntimeContext().getState(new ValueStateDescriptor<Long>("esTime", Long.class));
        }

        @Override
        public void processElement(JSONObject value, Context ctx, Collector<String> out) throws Exception {
            Integer messageFirst = messageCnt.value();

            Long times = esTime.value();
            Long ts = value.getLong("es");
            /*第一次注册定时器*/
            if (messageFirst == 0 && ts == null) {

                Long finishTs = ts + 6000L;
                ctx.timerService().registerEventTimeTimer(finishTs);
                esTime.update(finishTs);

            }
            messageCnt.update(messageFirst + 1);
            out.collect(value.getLong("es") + "," + messageCnt + "," + value.getLong("ts"));
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
            if (timestamp == (esTime.value())) {
                esTime.clear();
                messageCnt.clear();
            }


        }
    }

    /**
     * @Author ski_wangxuteng
     * @Date 18:11 2021/3/30
     * @Param
     * @return
     * @Description 注册累加器, 来一条进行加一;
     **/
    private static class MyAggregateFunction implements AggregateFunction<JSONObject, Long, Long> {
        @Override
        public Long createAccumulator() {
            return 0L;
        }

        @Override
        public Long add(JSONObject value, Long accumulator) {
            return accumulator + 1;
        }

        @Override
        public Long getResult(Long accumulator) {
            return accumulator;
        }

        @Override
        public Long merge(Long a, Long b) {
            return a + b;
        }
    }

}
