package com.yuanxin.real_time.flink_kafka

import java.util.Properties

import com.yuanxin.MyKafkaUtil
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}

/**
 *
 * @Package: com.yuanxin.real_time.flink_kafka
 * @ClassName: FLink12_kafak_test
 * @Author: ski_starLight
 * @CreateTime: 2021/3/31 11:01
 * @Description:
 */
object FLink12_kafak_test {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val value = env.fromCollection(List(1, 2, 5, 3, 5, 6))
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "192.168.3.102:9092")
    properties.setProperty("group.id", "test")
    val stream = env
      .addSource(new FlinkKafkaConsumer[String]("first", new SimpleStringSchema(), properties))
    stream.print()

    env.execute()
  }

}
