package com.yuanxin;

/**
 * @Package: com.yuanxin
 * @ClassName: MyKafkaUtil
 * @Author: ski_starLight
 * @CreateTime: 2021/3/29 14:24
 * @Description:
 */

import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.util.serialization.KeyedSerializationSchema;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Properties;

public class MyKafkaUtil {
    private static String kafkaServer = "192.168.3.102:9092";

    //封装Kafka消费者
    public static FlinkKafkaConsumer<String> getKafkaSource(String topic,String groupId){
        Properties prop = new Properties();
        prop.setProperty(ConsumerConfig.GROUP_ID_CONFIG,groupId);
        prop.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,kafkaServer);
        return new FlinkKafkaConsumer<String>(topic,new SimpleStringSchema(),prop);
    }
    //封装Kafka生产者
    public static FlinkKafkaProducer<String> getKafkaSink(String topic) {
        return new FlinkKafkaProducer<>(kafkaServer,topic,new SimpleStringSchema());
    }

}


class MyKeyedSchema implements   KeyedSerializationSchema<JSONObject>{

    @Override
    public byte[] serializeKey(JSONObject jsonObject) {
        return new byte[0];
    }

    @Override
    public byte[] serializeValue(JSONObject jsonObject) {
        return new byte[0];
    }

    @Override
    public String getTargetTopic(JSONObject jsonObject) {
        return null;
    }
}