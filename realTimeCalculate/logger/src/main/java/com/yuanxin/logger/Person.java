package com.yuanxin.logger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Package: com.yuanxin.logger
 * @ClassName: Person
 * @Author: ski_starLight
 * @CreateTime: 2021/3/15 19:02
 * @Description:
 */
@ConfigurationProperties(prefix = "person")
@Component
@Data
public class Person {

    private String lastName;
    private int age;
    private boolean boss;
    private Date birth;

    private Map<String,Object> maps;
    private List<String> lists;

    private Dog dog;


}
