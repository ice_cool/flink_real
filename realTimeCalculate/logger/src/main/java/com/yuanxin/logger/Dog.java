package com.yuanxin.logger;

/**
 * @Package: com.yuanxin.logger
 * @ClassName: Dog
 * @Author: ski_starLight
 * @CreateTime: 2021/3/15 19:02
 * @Description:
 */

public class Dog {

    private String name;

    private Integer age ;

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
